## Contents of this file

* [Introduction](#intro)
* [Requirements](#requirements)
* [Recommended modules](#recommended)
* [Installation](#install)
* [Configuration](#config)
* [Function](#func)
* [Maintainers](#maintainers)

## Introduction<a id="intro"></a>

This module implements a PET (*Privacy Enhanching Technology*) that
lets the user monitor robot activity on site.

It is part of *The PET project*.  For more information, visit [The PET
project website][01].

## Requirements<a id="requirements"></a>

* [Advanced help hint][03]:  
  To hint about how to get `README.md` displayed.

## Recommended modules<a id="recommended"></a>

* [Advanced Help][04]:  
  When this module is enabled, display of the project's `README.md`
  will be rendered when you visit
  `help/robotmonitor/README.md`.
* [Markdown filter][05]:  
  When this module is enabled, the project's `README.md` will be
  rendered with the markdown filter.

## Installation<a id="install"></a>

1. Install as you would normally install a contributed drupal
   module. See: [Installing modules][06] for further information.

2. Enable the **Robot monitor** module on the *Modules* list page.
   It appears in the “PET” section.

3. Proceed to configure the module as described in the configuraton
   section below.

## Configuration<a id="config"></a>

To alter the global settings for the module, navigate to
*Configuration » Web services » Robot monitor*.

You can alter the following settings:

* Treatment of anonymous user agents
* Simulate robot visit
* Paths to honey trap and permitted directories
* Bot identifiers.

## Function<a id="func"></a>

If you want to make use of this module, first make it a dependency in
your module's `.info`-file.  You do that with the following line:

    dependencies[] = robotmonitor

The module provides this function that can be helpful to other
projects:

    function robotmonitor_isuabot()

This function returns `true` if the UA is a robot (as determined by
the module), and `false` otherwise.

## Maintainers<a id="maintainers"></a>

* [gisle][08] - Gisle Hannemyr (creator and current maintainer)

Developement was sponsored by [Hannemyr Nye Medier AS][09].

Any help with development (patches, reviews, comments) are welcome.

[01]: https://pet.roztr.org/
[02]: https://www.duo.uio.no/handle/10852/37434
[03]: https://www.drupal.org/project/advanced_help_hint
[04]: https://www.drupal.org/project/advanced_help
[05]: https://www.drupal.org/project/markdown
[06]: https://drupal.org/documentation/install/modules-themes/modules-7

[08]: https://www.drupal.org/u/gisle
[09]: https://hannemyr.no



