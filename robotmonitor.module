<?php
/**
 * @file
 * Main hooks for Robot monitor.
 */

/**
 * The default bots list starts with three generic strings that are
 * part of many names used for bot UAs: "bot|crawler|spider", then it
 * lists strings that identify specific bots. We try to avoid
 * duplicates, so we look for "google", and not for
 * "mediapartners-google".
 */
$GLOBALS['default_bots'] = 'bot|crawler|spider|alexa|aolbuild|appie|baidu|bingpreview|butterfly|curl|duckduckgo|facebook|fast|favicon|firefly|froogle|galaxy|google|heritrix|infoseek|inktomi|jeeves|jigsaw|looksmart|me.dium|nationaldirectory|pingdom|postrank|rabaz|scooter|slurp|spade|technoratisnoop|tecnoseek|teoma|twiceler|twitter|twitturls|voyager|webalta|webbug|yandex|zelist|zyborg';

/**
 * Implements hook_help().
 */
function robotmonitor_help($path, $arg) {
  switch ($path) {
    case 'admin/help#robotmonitor':
      $helptext = t('<p>The <strong>Robot monitor</strong> module lets users monitor robot activity on site.</p>');
      $helptext .= '<p>' . advanced_help_hint_docs('robotmonitor', 'https://www.drupal.org/docs/7/modules/robotmonitor', TRUE) . '</p>';
      return $helptext;
      break;
  }
}

/**
 * Implements hook_menu().
 */
function robotmonitor_menu() {
  $items['admin/config/services/robotmonitor'] = array(
    'title' => 'Robot monitor',
    'description' => 'Configure global settings for Robot monitor',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('robotmonitor_settings'),
    'access arguments' => array('administer robotmonitor'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'robotmonitor.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function robotmonitor_permission() {
  return array(
    'administer robotmonitor' => array(
      'title' => t('administer robotmonitor'),
      'description' => t('Allows configuring module.'),
    ),
  );
}

/**
 * Helper functon to check if ua is a bot.
 *
 * @return
 *   Returns true if ua is a bot, otherwise false.
 */
function robotmonitor_isuabot() {
  $botlist = variable_get('robotmonitor_botlist', $GLOBALS['default_bots']);
  $ua = isset($_SERVER['HTTP_USER_AGENT']) ? check_plain($_SERVER['HTTP_USER_AGENT']) : null;
  $options = variable_get('robotmonitor_options');
  if ($options['anonuasetting']) {
    $len = strlen($ua);
    if ($len < 4) {
      return true;
    }
  }
  if (preg_match('/(' . $botlist . ')/i', $ua)) {
    return true;
  }
  return false;
}
