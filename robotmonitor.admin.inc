<?php
/**
 * @file
 * Constructs robotmonitor admin form.
 */

/**
 * Build form for admin/settings page.
 */
function robotmonitor_settings($form, &$form_state) {
   global $default_bots;

  $form = array();
  $form['robotmonitor_general'] = array(
    '#markup' => t('<p>These settings affect all instances of the Robot monitor module.</p>'),
  );

  $global_options = array(
    'anonuasetting' => t('Assume that anonymous user agents are robots.'),
    'simulaterobot' => t('Simulate robot visit'),
  );
  $global_ovalues = array(
    'anonuasetting' => 'anonuasetting',
    'simulaterobot' => 0,
  );

  $form['robotmonitor_options'] = array(
    '#title' => t('Global options'),
    '#type' => 'checkboxes',
    '#description' => t('Checking the first option will treat user agents that do not identify themselves as robots.<br />Checking the second option will let you see how xxx look like to robots.'),
    '#options' => $global_options,
    '#default_value' => variable_get('robotmonitor_options', $global_ovalues),
  );

  $form['robotmonitor_honeytrap'] = array(
    '#title' => t('Path to honey trap directory'),
    '#type' => 'textfield',
    '#description' => t('Enter the default path to use for the honey trap directory.'),
    '#default_value' => variable_get('robotmonitor_honeytrap', '/sites/default/files/honeytrap'),
    '#size' => 32,
    '#maxlength' => 128,
  );
  
  $form['robotmonitor_permitted'] = array(
    '#title' => t('Path to permitted directory'),
    '#type' => 'textfield',
    '#description' => t('Enter the default path to use for the permitted directory.'),
    '#default_value' => variable_get('robotmonitor_permitted', '/sites/default/files/permitted'),
    '#size' => 32,
    '#maxlength' => 128,
  );
  
  $form['robotmonitor_botlist'] = array(
    '#type' => 'textarea',
    '#title' => t('Bot identifiers'),
    '#default_value' => variable_get('robotmonitor_botlist', $GLOBALS['default_bots']),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('Enter list of strings that may be used to identify search engine robots by matching the string against the <code>HTTP_USER_AGENT</code> sent by the robot. Use no spaces, and use a vertical bar as separator.'),
  );
  return system_settings_form($form);
}
